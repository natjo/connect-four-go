package bot

import (
	"time"

	"gitlab.com/natjo/connect-four-go/config"
	"gitlab.com/natjo/connect-four-go/player"
	mcts "gitlab.com/natjo/gomcts"
)

const searchDepth int = 20
const explorationTime time.Duration = 1 * time.Second
const explorationParameter float64 = 1.44
const threads int = 8

type Bot struct {
	tree *mcts.Tree
}

func NewBot(config *config.Config, startingPlayer player.Player) *Bot {
	state := NewState(config)

	tree := mcts.NewTree(state, searchDepth, int(startingPlayer), explorationTime)
	tree.SetThreads(threads)
	tree.SetExplorationParameter(explorationParameter)
	tree.SetPrintLogs(false)

	return &Bot{tree: tree}
}

func (bot *Bot) GetMove() int {
	bot.tree.StartExplore()
	action, ok := bot.tree.GetBestAction().(*Action)
	if !ok {
		panic("Failed to understand response of bot")
	}
	return action.row
}

func (bot *Bot) MakeMove(x int, player player.Player) {
	action := NewAction(x, player)
	bot.tree.UpdateFromAction(action)
}

func (bot *Bot) GetWinningChances() map[int]float64 {
	winningChances := map[int]float64{}
	for action, chance := range bot.tree.GetScoresNext() {
		winningChances[action.(*Action).row] = chance
	}
	return winningChances
}

func (bot *Bot) GetNumPlayouts() map[int]int {
	numPlayouts := map[int]int{}
	for action, numPlayout := range bot.tree.GetNumPlayoutsNext() {
		numPlayouts[action.(*Action).row] = numPlayout
	}
	return numPlayouts
}
