package bot

import (
	"gitlab.com/natjo/connect-four-go/config"
	"gitlab.com/natjo/connect-four-go/logic"
	mcts "gitlab.com/natjo/gomcts"
)

type State struct {
	game *logic.Game
}

func NewState(config *config.Config) *State {
	return &State{game: logic.NewGame(config)}
}

func (s *State) Apply(a mcts.Action) mcts.Outcome {
	// Conversion from interface to actual type in this module is always required
	action := a.(*Action)
	s.game.SetToken(action.row)
	winner, isTie := s.game.GetWinner()

	if isTie {
		return mcts.Tie
	} else if int(winner) == a.GetActor() {
		return mcts.Win
	}
	return mcts.NotFinal
}

func (s *State) GetNextActions() []mcts.Action {
	optionalRows := s.game.GetNotFullRows()
	actions := []mcts.Action{}
	for _, row := range optionalRows {
		newAction := NewAction(row, s.game.GetCurrentPlayer())
		actions = append(actions, newAction)
	}
	return actions
}

func (s *State) Clone() mcts.State {
	return &State{game: s.game.Clone()}
}
