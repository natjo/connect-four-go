package bot

import (
	"gitlab.com/natjo/connect-four-go/player"
	mcts "gitlab.com/natjo/gomcts"
)

type Action struct {
	row    int
	player player.Player
}

func NewAction(row int, player player.Player) *Action {
	return &Action{row: row, player: player}
}

func (a *Action) GetActor() int {
	return int(a.player)
}

func (a *Action) IsEqual(cmpA mcts.Action) bool {
	cmpAction := cmpA.(*Action)
	return a.row == cmpAction.row && a.player == cmpAction.player
}
