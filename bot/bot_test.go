package bot

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/natjo/connect-four-go/config"
	"gitlab.com/natjo/connect-four-go/player"
	mcts "gitlab.com/natjo/gomcts"
)

func getTestBot() *Bot {
	// Returns a simplified bot, that runs for a short duration and prints logs
	startingPlayer := player.P0
	config := config.NewConfig()
	explorationTime := 200 * time.Millisecond
	searchDepth := 3
	bot := NewBot(config, player.P0)
	bot.tree = mcts.NewTree(NewState(config), searchDepth, int(startingPlayer), explorationTime)
	bot.tree.SetPrintLogs(true)
	return bot
}

func TestNoPanic(t *testing.T) {
	// Given
	bot := getTestBot()

	// When
	// xxxxxxx
	// xxxxxxx
	// xxxxxxx
	// xxxxxxx
	// xxxxxxx
	// 0xxxxxx
	bot.MakeMove(1, player.P0)

	// Then
	assert.NotPanics(t, func() { bot.GetMove() })
}

func TestPlaceHorizontalWin(t *testing.T) {
	// Given
	bot := getTestBot()

	// When
	// xxxxxxx
	// xxxxxxx
	// xxxxxxx
	// 10xxxxx
	// 10xxxxx
	// 100xxxx
	bot.MakeMove(1, player.P0)
	bot.MakeMove(0, player.P1)
	bot.MakeMove(1, player.P0)
	bot.MakeMove(0, player.P1)
	bot.MakeMove(1, player.P0)
	bot.MakeMove(0, player.P1)
	bot.MakeMove(2, player.P0)

	// Then
	x := bot.GetMove()
	assert.Equal(t, 0, x)
}
