package player

type Player uint8

const (
	// None must be first, so it's the default
	None Player = iota
	P0
	P1
)

func NextPlayer(currentPlayer Player) Player {
	if currentPlayer == P0 {
		return P1
	}
	return P0
}
