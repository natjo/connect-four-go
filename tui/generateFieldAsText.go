package tui

import (
	"bytes"

	"gitlab.com/natjo/connect-four-go/player"
)

func generateFieldAsText(gameCtrl GameCtrl) string {
	var buffer bytes.Buffer

	// Top line
	buffer.WriteString("┏")
	for x := 0; x < gameCtrl.GetWidth()-1; x++ {
		buffer.WriteString("━┳")
	}
	buffer.WriteString("━┓\n")

	// Main Field
	// for y := 0; y < gameCtrl.GetHeight(); y++ {
	for y := gameCtrl.GetHeight() - 1; y >= 0; y-- {
		// Tokens
		buffer.WriteString("┃")
		for x := 0; x < gameCtrl.GetWidth(); x++ {
			token := getTokenOnField(x, y, gameCtrl)
			buffer.WriteString(token)
			buffer.WriteString("┃")
		}
		buffer.WriteString("\n")

		// Line
		if y > 0 {
			buffer.WriteString("┣")
			for range [6]int{} {
				buffer.WriteString("━╋")
			}
			buffer.WriteString("━┫\n")
		}
	}

	// Bottom Line
	buffer.WriteString("┗")
	for y := 0; y < gameCtrl.GetHeight(); y++ {
		buffer.WriteString("━┻")
	}
	buffer.WriteString("━┛\n")
	buffer.WriteString(" 0 1 2 3 4 5 6 ")
	return buffer.String()
}

func getTokenOnField(x, y int, gameCtrl GameCtrl) string {
	switch gameCtrl.GetToken(x, y) {
	case player.None:
		return " "
	case player.P0:
		return "[#ffff00]⬣[white]"
	case player.P1:
		return "[#ff0000]⬣[white]"
	default:
		panic("Tried to display token of unknown error")
	}
}
