package tui

import (
	"fmt"
	"strconv"
	"strings"

	"github.com/gdamore/tcell/v2"
	"github.com/rivo/tview"
	"gitlab.com/natjo/connect-four-go/player"
)

type TUI struct {
	app             *tview.Application
	gameCtrl        GameCtrl
	fieldView       *tview.TextView
	probabilityView *tview.TextView
	logView         *tview.TextView
	inputField      *tview.InputField
}

type GameCtrl interface {
	GetToken(column, row int) player.Player
	SetToken(column int) (int, error)
	GetWidth() int
	GetHeight() int
	GetWinner() (player.Player, bool)
	GetWinningChancesBot() []float64
	GetNumPlayoutsBot() []int
}

func NewTUI(gameCtrl GameCtrl) *TUI {
	tui := &TUI{gameCtrl: gameCtrl}
	tui.initApp()
	return tui
}

func (tui *TUI) Run() {
	if err := tui.app.Run(); err != nil {
		panic(err)
	}
}

// Set general views
// Requires field to already be set
func (tui *TUI) initApp() {
	tui.app = tview.NewApplication()
	header := tui.createHeader()
	probabilityView := tui.createProbabilityDisplay()
	main := tui.createMain()
	footer := tui.createFooter()

	grid := tview.NewGrid().
		SetRows(3, 4, 0, 3).
		SetColumns(0).
		SetBorders(true).
		AddItem(header, 0, 0, 1, 3, 0, 0, false).
		AddItem(probabilityView, 1, 0, 1, 3, 0, 0, false).
		AddItem(main, 2, 0, 1, 3, 0, 0, false).
		AddItem(footer, 3, 0, 1, 3, 0, 0, true)

	tui.app.SetRoot(grid, true)
}

func (tui *TUI) createHeader() tview.Primitive {
	return center(0, 1, tview.NewTextView().
		SetTextAlign(tview.AlignCenter).
		SetDynamicColors(true).
		SetText("Connect Four!"))
}

func (tui *TUI) createProbabilityDisplay() tview.Primitive {
	tui.probabilityView = tview.NewTextView().
		SetTextAlign(tview.AlignCenter).
		SetDynamicColors(true).
		SetText("")
	tui.updateWinningProbabilities()
	return tui.probabilityView
}

func (tui *TUI) createMain() tview.Primitive {
	tui.fieldView = tview.NewTextView().
		SetDynamicColors(true)
	tui.updateField()
	return center(tui.gameCtrl.GetWidth()*2+1, tui.gameCtrl.GetHeight()*2+1+1, tui.fieldView)
}

func (tui *TUI) createFooter() tview.Primitive {
	tui.logView = tview.NewTextView().
		SetDynamicColors(true).
		SetText("")
	tui.inputField = tview.NewInputField().
		SetLabel("Place token in row: ").
		SetFieldWidth(2).
		SetAcceptanceFunc(tview.InputFieldInteger)
	tui.inputField.SetDoneFunc(func(key tcell.Key) {
		if key == tcell.KeyEnter {
			posX, err := strconv.Atoi(tui.inputField.GetText())
			if err != nil {
				tui.log(err.Error())
			} else {
				tui.handleRowSelection(posX)
			}
			tui.inputField.SetText("")
		}
	})
	return tview.NewFlex().
		SetDirection(tview.FlexRow).
		AddItem(tui.inputField, 0, 1, true).
		AddItem(tui.logView, 0, 1, false)
}

func (tui *TUI) handleRowSelection(x int) {
	y, err := tui.gameCtrl.SetToken(x)
	if err != nil {
		// Unallowed move
		tui.log(err.Error())
		return
	}
	// Successful move
	tui.updateWinningProbabilities()
	isOver := tui.executeMove(x, y)
	if isOver {
		tui.inputField.SetDisabled(true)
	}
}

func (tui *TUI) executeMove(posX, posY int) bool {
	tui.updateField()
	tui.log(fmt.Sprintf("Placed token at %d|%d", posX, posY))
	isOver := tui.isOver()
	return isOver
}

func (tui *TUI) isOver() bool {
	winner, isTie := tui.gameCtrl.GetWinner()
	if winner != player.None {
		tui.log(fmt.Sprintf("Player %d won!", winner))
		return true
	}
	if isTie {
		tui.log("No more empty spots, it's a tie!")
		return true
	}
	return false
}

func (tui *TUI) log(msg string) {
	oldMsg := strings.Split(tui.logView.GetText(false), "\n")[0]
	msg = fmt.Sprintf("%s\n%s", msg, oldMsg)
	tui.logView.SetText(msg)
}

func (tui *TUI) updateField() {
	tui.fieldView.SetText(generateFieldAsText(tui.gameCtrl))
}

func (tui *TUI) updateWinningProbabilities() {
	probs := tui.generateWinningProbabilitiesString()
	tui.probabilityView.SetText(probs)
}

func (tui *TUI) generateWinningProbabilitiesString() string {
	header := "|"
	separator := "\n|"
	probs := "\n|"
	numPlayoutsString := "\n|"
	numPlayouts := tui.gameCtrl.GetNumPlayoutsBot()
	for i, p := range tui.gameCtrl.GetWinningChancesBot() {
		header += fmt.Sprintf("   %d   |", i)
		separator += "-------|"
		probs += fmt.Sprintf(" %5.2f |", p*100)
		numPlayoutsString += fmt.Sprintf(" %5d |", numPlayouts[i])
	}
	return header + separator + probs + numPlayoutsString
}
