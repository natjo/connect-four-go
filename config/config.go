package config

import "gitlab.com/natjo/connect-four-go/player"

// If BotPlayer is player.None, both players will be human
type Config struct {
	Width      int
	Height     int
	InitPlayer player.Player
	BotPlayer  player.Player
}

func NewConfig() *Config {
	return &Config{Width: 7, Height: 6, InitPlayer: player.P0, BotPlayer: player.P1}
}

func NewConfigNoBot() *Config {
	return &Config{Width: 7, Height: 6, InitPlayer: player.P0, BotPlayer: player.None}
}
