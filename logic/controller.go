package logic

import (
	"gitlab.com/natjo/connect-four-go/config"
	"gitlab.com/natjo/connect-four-go/player"
)

type Controller struct {
	game              *Game
	botPlayer         player.Player
	bot               Bot
	winningChancesBot []float64
	numPlayoutsBot    []int
}

type Bot interface {
	GetMove() int
	MakeMove(int, player.Player)
	GetWinningChances() map[int]float64
	GetNumPlayouts() map[int]int
}

func NewController(config *config.Config, bot Bot) *Controller {
	return &Controller{
		game:      NewGame(config),
		botPlayer: config.BotPlayer,
		bot:       bot,
	}
}

func (ctrl *Controller) GetToken(x, y int) player.Player {
	return ctrl.game.GetToken(x, y)
}

func (ctrl *Controller) SetToken(x int) (int, error) {
	y, err := ctrl.game.SetToken(x)
	if err != nil {
		// Move was not valid, return error, do not advance game
		return y, err
	}

	// Update bot
	ctrl.updateBot(x)

	// Move was valid, check if next move must be done by bot
	if !ctrl.game.IsGameOver() {
		ctrl.MakeOptionalBotMove()
	}
	return y, err
}

func (ctrl *Controller) MakeOptionalBotMove() {
	if ctrl.game.GetCurrentPlayer() == ctrl.botPlayer {
		x := ctrl.bot.GetMove()
		ctrl.storeWinningChancesBot()
		ctrl.storeNumPlayoutsBot()
		_, err := ctrl.SetToken(x)
		if err != nil {
			panic(err)
		}
	}
}

func (ctrl *Controller) GetWinner() (winner player.Player, isTie bool) {
	return ctrl.game.GetWinner()
}

func (ctrl *Controller) GetWidth() int {
	return ctrl.game.GetWidth()
}

func (ctrl *Controller) GetHeight() int {
	return ctrl.game.GetHeight()
}

func (ctrl *Controller) storeWinningChancesBot() {
	winningChancesMap := ctrl.bot.GetWinningChances()
	winningChances := []float64{}
	for i := 0; i < ctrl.game.GetWidth(); i++ {
		chance, ok := winningChancesMap[i]
		if !ok {
			chance = 0
		}
		winningChances = append(winningChances, chance)
	}
	ctrl.winningChancesBot = makeAbsSumOne(winningChances)
}

func (ctrl *Controller) GetWinningChancesBot() []float64 {
	return ctrl.winningChancesBot
}

func (ctrl *Controller) storeNumPlayoutsBot() {
	numPlayoutsMap := ctrl.bot.GetNumPlayouts()
	numPlayouts := []int{}
	for i := 0; i < ctrl.game.GetWidth(); i++ {
		numPlayout, ok := numPlayoutsMap[i]
		if !ok {
			numPlayout = 0
		}
		numPlayouts = append(numPlayouts, numPlayout)
	}
	ctrl.numPlayoutsBot = numPlayouts
}

func (ctrl *Controller) GetNumPlayoutsBot() []int {
	return ctrl.numPlayoutsBot
}

func (ctrl *Controller) updateBot(x int) {
	if ctrl.botPlayer != player.None {
		ctrl.bot.MakeMove(x, ctrl.game.GetCurrentPlayer())
	}
}
