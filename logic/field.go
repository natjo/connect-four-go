package logic

import (
	"fmt"

	"gitlab.com/natjo/connect-four-go/player"
)

type Field struct {
	// x=0 & y=0 is bottom left
	tiles  [][]player.Player
	Width  int
	Height int
}

func NewField(width, height int) *Field {
	tiles := make([][]player.Player, height)
	for i := range tiles {
		// Default value of Player is None, so we can initialize with the default
		tiles[i] = make([]player.Player, width)
	}
	return &Field{tiles: tiles, Width: width, Height: height}
}
func (field *Field) Clone() *Field {
	clonedTiles := make([][]player.Player, len(field.tiles))
	for i := range field.tiles {
		clonedTiles[i] = make([]player.Player, len(field.tiles[i]))
		copy(clonedTiles[i], field.tiles[i])
	}
	return &Field{tiles: clonedTiles, Width: field.Width, Height: field.Height}
}

func (field *Field) GetToken(x, y int) player.Player {
	return field.tiles[y][x]
}

func (field *Field) IsEmpty(x, y int) bool {
	return field.tiles[y][x] == player.None
}

func (field *Field) isToken(x, y int, p player.Player) bool {
	return field.GetToken(x, y) == p
}

func (field *Field) SetToken(x int, p player.Player) (int, error) {
	if x < 0 || x >= field.Width {
		return 0, fmt.Errorf("row %d outside field", x)
	}
	y, err := field.findEmptyYInCol(x)
	field.tiles[y][x] = p
	return y, err
}

func (field *Field) findEmptyYInCol(x int) (int, error) {
	y := field.Height / 2
	if field.IsEmpty(x, y) {
		// current tile is empty, go down until you find a token
		y--
		for y >= 0 && field.IsEmpty(x, y) {
			y--
		}
		// Add one, since breaking condition on bottom is at -1, i.e. after we checked the bottom tile
		return y + 1, nil
	} else {
		// current tile is not empty, go up until you find an empty one
		y++
		for !field.IsEmpty(x, y) {
			y++
			if y >= field.Height {
				return 0, fmt.Errorf("no free spot in column %d left", x)
			}
		}
		return y, nil
	}
}
