package logic

import (
	"fmt"

	"gitlab.com/natjo/connect-four-go/config"
	"gitlab.com/natjo/connect-four-go/player"
)

type Game struct {
	field         *Field
	currentPlayer player.Player
	lastPosX      int
	lastPosY      int
	winner        player.Player
	isTie         bool
}

func NewGame(config *config.Config) *Game {
	return &Game{
		field:         NewField(config.Width, config.Height),
		currentPlayer: config.InitPlayer,
		winner:        player.None,
		isTie:         false,
	}
}

func (game *Game) Clone() *Game {
	return &Game{
		field:         game.field.Clone(),
		currentPlayer: game.currentPlayer,
		winner:        game.winner,
		isTie:         game.isTie,
	}
}

func (game *Game) GetToken(x, y int) player.Player {
	return game.field.GetToken(x, y)
}

func (game *Game) SetToken(x int) (int, error) {
	y, err := game.field.SetToken(x, game.currentPlayer)
	if err != nil {
		return y, err
	}

	// Move was valid, update to next player, store last move
	game.lastPosX = x
	game.lastPosY = y
	game.winner, game.isTie = game.checkForWinner()
	game.currentPlayer = player.NextPlayer(game.currentPlayer)

	return y, err
}

func (game *Game) GetWidth() int {
	return game.field.Width
}

func (game *Game) GetHeight() int {
	return game.field.Height
}

func (game *Game) GetCurrentPlayer() player.Player {
	return game.currentPlayer
}

func (game *Game) IsGameOver() bool {
	return game.winner != player.None || game.isTie
}

func (game *Game) GetWinner() (winner player.Player, isTie bool) {
	return game.winner, game.isTie
}

func (game *Game) GetNotFullRows() []int {
	notFullRows := []int{}
	maxY := game.field.Height - 1
	for x := 0; x < game.field.Width; x++ {
		if game.field.IsEmpty(x, maxY) {
			notFullRows = append(notFullRows, x)
		}
	}
	return notFullRows
}

func (game *Game) checkForWinner() (winner player.Player, isTie bool) {
	// Check for tie first
	if game.checkForTie() {
		return player.None, true
	}

	// Horizontal check
	winner = game.checkForWinnerVertical()
	if winner != player.None {
		return winner, false
	}

	// Vertical check
	winner = game.checkForWinnerHorizontal()
	if winner != player.None {
		return winner, false
	}

	// Diag BotLeft to TopRight check
	winner = game.checkForWinnerDiagBotLeftTopRight()
	if winner != player.None {
		return winner, false
	}

	// Diag BotRight to TopLeft check
	winner = game.checkForWinnerDiagBotRightTopLeft()
	if winner != player.None {
		return winner, false
	}

	// Not decided yet
	return player.None, false
}

func (game *Game) checkForTie() bool {
	// Check if at least one empty field in the top row exists.
	// As long as one can play a piece, it's no tie yet
	for x := 0; x < game.field.Width; x++ {
		if game.field.IsEmpty(x, game.field.Height-1) {
			return false
		}
	}
	return true
}

func (game *Game) checkForWinnerVertical() player.Player {
	totalInRow := 1
	// Go down
	for y := game.lastPosY - 1; y >= 0 && game.field.isToken(game.lastPosX, y, game.currentPlayer); y-- {
		totalInRow++
	}
	if totalInRow >= 4 {
		// Found a winner
		return game.currentPlayer
	}
	// Not decided yet
	return player.None
}

func (game *Game) checkForWinnerHorizontal() player.Player {
	totalInRow := 1
	// Go left
	for x := game.lastPosX - 1; x >= 0 && game.field.isToken(x, game.lastPosY, game.currentPlayer); x-- {
		totalInRow++
	}
	// Go right
	for x := game.lastPosX + 1; x < game.field.Width && game.field.isToken(x, game.lastPosY, game.currentPlayer); x++ {
		totalInRow++
	}
	if totalInRow >= 4 {
		// Found a winner
		return game.currentPlayer
	}
	// Not decided yet
	return player.None
}

func (game *Game) checkForWinnerDiagBotLeftTopRight() player.Player {
	totalInRow := 1
	// Go bot left
	for x, y := game.lastPosX-1, game.lastPosY-1; x >= 0 && y >= 0 && game.field.isToken(x, y, game.currentPlayer); {
		totalInRow++
		x--
		y--
	}
	// Go top right
	for x, y := game.lastPosX+1, game.lastPosY+1; x < game.field.Width && y < game.field.Height && game.field.isToken(x, y, game.currentPlayer); {
		totalInRow++
		x++
		y++
	}
	fmt.Sprintln(totalInRow)
	if totalInRow >= 4 {
		// Found a winner
		return game.currentPlayer
	}
	// Not decided yet
	return player.None
}

func (game *Game) checkForWinnerDiagBotRightTopLeft() player.Player {
	totalInRow := 1
	// Go bot right
	for x, y := game.lastPosX+1, game.lastPosY-1; x < game.field.Width && y >= 0 && game.field.isToken(x, y, game.currentPlayer); {
		totalInRow++
		x++
		y--
	}
	// Go top left
	for x, y := game.lastPosX-1, game.lastPosY+1; x >= 0 && y < game.field.Height && game.field.isToken(x, y, game.currentPlayer); {
		totalInRow++
		x--
		y++
	}
	fmt.Sprintln(totalInRow)
	if totalInRow >= 4 {
		// Found a winner
		return game.currentPlayer
	}
	// Not decided yet
	return player.None
}
