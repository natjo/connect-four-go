package logic

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/natjo/connect-four-go/config"
	"gitlab.com/natjo/connect-four-go/player"
)

func TestGetNotFullRowsAll(t *testing.T) {
	// Given
	game := NewGame(config.NewConfigNoBot())

	// When
	// xxxxxxx
	// xxxxxxx
	// xxxxxxx
	// xxxxxxx
	// xxxxxxx
	// 0xxxxxx
	game.SetToken(0)

	// Then
	notFullRows := game.GetNotFullRows()
	expected := []int{0, 1, 2, 3, 4, 5, 6}
	assert.ElementsMatch(t, expected, notFullRows)
}

func TestGetNotFullRowsFirstFull(t *testing.T) {
	// Given
	game := NewGame(config.NewConfigNoBot())

	// When
	// 1xxxxxx
	// 00xxxxx
	// 11xxxxx
	// 00xxxxx
	// 11xxxxx
	// 00xxxxx
	game.SetToken(0)
	game.SetToken(0)
	game.SetToken(0)
	game.SetToken(0)
	game.SetToken(0)
	game.SetToken(0)
	game.SetToken(1)
	game.SetToken(1)
	game.SetToken(1)
	game.SetToken(1)
	game.SetToken(1)

	// Then
	notFullRows := game.GetNotFullRows()
	expected := []int{1, 2, 3, 4, 5, 6}
	assert.ElementsMatch(t, expected, notFullRows)
}

func TestNoTie(t *testing.T) {
	// Given
	game := NewGame(config.NewConfigNoBot())

	// When
	// xxxxxxx
	// xxxxxxx
	// xxxxxxx
	// xxxxxxx
	// xxxxxxx
	// 0xxxxxx
	game.SetToken(0)

	// Then
	isTie := game.checkForTie()
	assert.False(t, isTie)
}

func TestTie(t *testing.T) {
	// Given
	game := NewGame(config.NewConfigNoBot())

	// When
	// 1010101
	// 1010100
	// 1010101
	// 0101010
	// 0101011
	// 0101010
	piecesToPlay := []int{
		0, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 0,
		2, 3, 2, 3, 2, 3, 3, 2, 3, 2, 3, 2,
		4, 5, 4, 5, 4, 5, 5, 4, 5, 4, 5, 4,
		6, 6, 6, 6, 6, 6,
	}
	for _, i := range piecesToPlay {
		game.SetToken(i)
	}

	// Then
	isTie := game.checkForTie()
	assert.True(t, isTie)
}

func TestNoWin(t *testing.T) {
	// Given
	game := NewGame(config.NewConfigNoBot())

	// When
	// xxxxxxx
	// x1xxxxx
	// x1xxxxx
	// x00xxxx
	// 100xxxx
	// 1000111
	game.SetToken(1)
	game.SetToken(0)
	game.SetToken(1)
	game.SetToken(0)
	game.SetToken(1)
	game.SetToken(1)
	game.SetToken(2)
	game.SetToken(1)
	game.SetToken(2)
	game.SetToken(4)
	game.SetToken(2)
	game.SetToken(5)
	game.SetToken(3)
	game.SetToken(6)

	// Then
	winner, isTie := game.GetWinner()
	assert.Equal(t, player.None, winner)
	assert.False(t, isTie)
}

func TestNoWin2(t *testing.T) {
	// Given
	game := NewGame(config.NewConfigNoBot())

	// When
	// xxxxxxx
	// xxxxxxx
	// xxxxxxx
	// xxxxxxx
	// xxxxxxx
	// xxxxxxx
	game.SetToken(1)

	// Then
	winner, isTie := game.GetWinner()
	assert.Equal(t, player.None, winner)
	assert.False(t, isTie)
}
