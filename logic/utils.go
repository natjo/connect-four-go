package logic

// This function turns a set of postive/negative scores into a range of 0 to 1, with 0 being the lowest
// Return value is only for convenience, this function operates on the input slice directly
func makeAbsSumOne(s []float64) []float64 {
	min := min(s)
	for i := range s {
		s[i] -= min
	}
	sum := sum(s)
	if sum == 0 {
		sum = 1
	}
	for i := range s {
		s[i] /= sum
	}
	return s
}

func sum(s []float64) float64 {
	sum := 0.0
	for _, x := range s {
		sum += x
	}
	return sum
}

func min(s []float64) float64 {
	min := s[0]
	for _, x := range s[1:] {
		if x < min {
			min = x
		}
	}
	return min
}
