package logic

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNormalize(t *testing.T) {
	// Given
	s := []float64{0.0, 1.0, 2.0, 1.0}

	// When
	makeAbsSumOne(s)

	// Then
	expected := []float64{0, 0.25, 0.5, 0.25}
	assert.ElementsMatch(t, expected, s)
}

func TestNormalizeWithNegative(t *testing.T) {
	// Given
	s := []float64{0.0, -1.0, 6.0, 1.0}

	// When
	makeAbsSumOne(s)

	// Then
	expected := []float64{0.1, 0.0, 0.7, 0.2}
	assert.ElementsMatch(t, expected, s)
}
