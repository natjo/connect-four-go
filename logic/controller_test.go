package logic

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/natjo/connect-four-go/config"
	"gitlab.com/natjo/connect-four-go/player"
)

func TestWinVertical(t *testing.T) {
	// Given
	ctrl := NewController(config.NewConfigNoBot(), nil)

	// When
	// xxxxxxx
	// xxxxxxx
	// 0xxxxxx
	// 01xxxxx
	// 01xxxxx
	// 01xxxxx
	ctrl.SetToken(0)
	ctrl.SetToken(1)
	ctrl.SetToken(0)
	ctrl.SetToken(1)
	ctrl.SetToken(0)
	ctrl.SetToken(1)
	ctrl.SetToken(0)

	// Then
	winner, _ := ctrl.GetWinner()
	assert.Equal(t, player.P0, winner)
}

func TestWinHorizontal(t *testing.T) {
	// Given
	ctrl := NewController(config.NewConfigNoBot(), nil)

	// When
	// xxxxxxx
	// xxxxxxx
	// xxxxxxx
	// xxxxx0x
	// xxxxx0x
	// 011110x
	ctrl.SetToken(0)
	ctrl.SetToken(1)
	ctrl.SetToken(5)
	ctrl.SetToken(2)
	ctrl.SetToken(5)
	ctrl.SetToken(3)
	ctrl.SetToken(5)
	ctrl.SetToken(4)

	// Then
	winner, _ := ctrl.GetWinner()
	assert.Equal(t, player.P1, winner)
}

func TestWinDiagBotLeftToTopRight(t *testing.T) {
	// Given
	ctrl := NewController(config.NewConfigNoBot(), nil)

	// When
	// xxxxxxx
	// xxxxxxx
	// xxx0xxx
	// xx010xx
	// x01101x
	// 011100x
	ctrl.SetToken(0)
	ctrl.SetToken(1)
	ctrl.SetToken(4)
	ctrl.SetToken(2)
	ctrl.SetToken(4)
	ctrl.SetToken(2)
	ctrl.SetToken(2)
	ctrl.SetToken(3)
	ctrl.SetToken(4)
	ctrl.SetToken(3)
	ctrl.SetToken(5)
	ctrl.SetToken(3)
	ctrl.SetToken(3)
	ctrl.SetToken(5)
	ctrl.SetToken(1)

	// Then
	winner, _ := ctrl.GetWinner()
	assert.Equal(t, player.P0, winner)
}

func TestWinDiagBotLeftToTopRight2(t *testing.T) {
	// Given
	ctrl := NewController(config.NewConfigNoBot(), nil)

	// When
	// xxxxxxx
	// xxxx1xx
	// xxx11xx
	// xx1000x
	// x10010x
	// x001110
	ctrl.SetToken(1)
	ctrl.SetToken(1)
	ctrl.SetToken(2)
	ctrl.SetToken(3)
	ctrl.SetToken(2)
	ctrl.SetToken(2)
	ctrl.SetToken(3)
	ctrl.SetToken(4)
	ctrl.SetToken(3)
	ctrl.SetToken(4)
	ctrl.SetToken(4)
	ctrl.SetToken(5)
	ctrl.SetToken(5)
	ctrl.SetToken(4)
	ctrl.SetToken(5)
	ctrl.SetToken(4)
	ctrl.SetToken(6)
	ctrl.SetToken(3)

	// Then
	winner, _ := ctrl.GetWinner()
	assert.Equal(t, player.P1, winner)
}

func TestWinDiagBotRightToTopLeft(t *testing.T) {
	// Given
	ctrl := NewController(config.NewConfigNoBot(), nil)

	// When
	// xxxxxxx
	// xx1xxxx
	// xx11xxx
	// x0001xx
	// x01001x
	// 011100x
	ctrl.SetToken(5)
	ctrl.SetToken(5)
	ctrl.SetToken(4)
	ctrl.SetToken(3)
	ctrl.SetToken(4)
	ctrl.SetToken(4)
	ctrl.SetToken(3)
	ctrl.SetToken(2)
	ctrl.SetToken(3)
	ctrl.SetToken(2)
	ctrl.SetToken(2)
	ctrl.SetToken(1)
	ctrl.SetToken(1)
	ctrl.SetToken(2)
	ctrl.SetToken(1)
	ctrl.SetToken(2)
	ctrl.SetToken(0)
	ctrl.SetToken(3)

	// Then
	winner, _ := ctrl.GetWinner()
	assert.Equal(t, player.P1, winner)
}
