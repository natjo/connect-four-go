package main

import (
	"gitlab.com/natjo/connect-four-go/bot"
	"gitlab.com/natjo/connect-four-go/config"
	"gitlab.com/natjo/connect-four-go/logic"
	"gitlab.com/natjo/connect-four-go/player"
	"gitlab.com/natjo/connect-four-go/tui"
)

func main() {
	config := config.NewConfig()
	bot := bot.NewBot(config, player.P0)
	gameCtrl := logic.NewController(config, bot)
	tui := tui.NewTUI(gameCtrl)
	tui.Run()
}
