module gitlab.com/natjo/connect-four-go

go 1.21.5

require (
	github.com/gdamore/tcell/v2 v2.7.1
	github.com/rivo/tview v0.0.0-20240413115534-b0d41c484b95
	github.com/stretchr/testify v1.9.0
	gitlab.com/natjo/gomcts v1.1.0
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/gdamore/encoding v1.0.0 // indirect
	github.com/lucasb-eyer/go-colorful v1.2.0 // indirect
	github.com/mattn/go-runewidth v0.0.15 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/rivo/uniseg v0.4.7 // indirect
	golang.org/x/sys v0.17.0 // indirect
	golang.org/x/term v0.17.0 // indirect
	golang.org/x/text v0.14.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
